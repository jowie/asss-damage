/* damage by smong
 * based on bot1 by smong
 * some additions by JoWie
 *
 * arena settings:
 *

[Damage]
IgnoreTeamDamage = 0

 *
 *
 * Stuff it does not do (yet):
 *
 *   when receiving a weapon with a timestamp in the past, keep updating it until it gets to the proper time 
 *   full support for all special tiles
 *   friendly bricks
 *   player interpolation (http://www.ssforum.net/index.php?showtopic=19720 ?)
 *   multifire
 *   shrapnel (+random shrapnel)
 *   burst
 *   repel
 *   wormhole gravity
 *
 *
 * apr 15 2005 smong (attempt to fix ?addbot def param freq bug)
 *
 * jan 16 2006 smong proper cleanup of turret list on unload
 *
 * jan 17 2006 smong damage, a bot1 mod.
 *
 * jan 28 2006 smong do cb_kill on <= 142
 *
 * feb 23 2006 smong increased enter delay from 200 to 400
 *                   energy is halfway between initial and maximum
 *                   ship defaults to warbird settings if starting in spec
 *
 * jun 14 2006 smong updated ASSSVERSION_NUM for 143
 *
 * jun 16 2006 smong fixed prox to use pid of triggerer.
 *
 * jun 17 2006 smong fixed bomb blast radius (-almost pulling hair out-).
 *
 * jun 25 2006 smong made max energy get reloaded on ship change.
 *                   changed energy level to use initial energy.
 *
 * jul 25 2006 smong fixed crashing in windows on arena creation (woo).
 *                   added support for tiles to take damage (direct hits only).
 *
 * may 16 2007 smong fixed crash when bot respawns in spec.
 *                   made the kill callback asynchronous so it happens outside
 *                   the mutex, allows add/remove tracking to be called.
 *                   recoded respawn_timer to use bd as the key, so the timer
 *                   can be cancelled if the bot is removed in the mean time.
 *
 * may 18 2007 smong fixed bots not reloading settings when spawning out of spec
 *
 * jun 6 2007 smong made the tile damage callback asynchronous so it happens
 *                  outside the mutex, allows add/remove tracking to be called.
 *                  moved mutex destroy to AA_POSTDESTROY to avoid crashes.
 *
 * jul 4 2007 smong added some support for the special tiles
 *                  added setting to ignore team damage
 *                  small tweaks to the map collision routines
 *
 * sep 15 2007 smong updated ASSSVERSION_NUM for 144
 *
 * juli 26 2009 JoWie - Fixed EMP bomb and bouncing bomb damage;
 *                      (emp bombs were normal damage;
 *                       bouncing bomb would do normal damage after the last bounce;
 *                        bouncing emp bomb used EBombDamagePercent and not EBombDamagePercent WITH BBombDamagePercent)
 *                      Region damage tracking now uses EBombDamagePercent and BBombDamagePercent
 *                      Added EMP Bomb Shutdown
 * juli 27 2009 JoWie - Added additional parameters to TileDamageFunc
 *                      Added FakeDamageFunc
 *                      Added manageenergy settings for fake players
 *                      ticks_t values now use the TICK_DIFF, TICK_MAKE, TICK_GT macro's
 *                      moved all cfg->GetInt to AA_CREATE | AA_CONFCHANGED instead of reading it constantly
 * feb 4 2010   JoWie - ASSS 1.5.0rc2 support
 *                      Added a check so that dead players can not collide with bombs / bullets
 * feb 21 2010  JoWie - Added Double Barrel support
 * apr 24 2010  JoWie - Now uses the CB_PPK callback so that position packets may be rewritten (there is also no more need for damage->FakePosition)
 * may 24 2010  JoWie - Slight overhaul of existing interface + additions to allow other modules to look up weapon info (but not modify)
 * jun 22 2010  JoWie - Fixed a bug with ?insmod and ?rmmod while there are existing arena's
 * okt 16 2010  JoWie - A_KILL support 
 */


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>
#include <string.h>

#include "asss.h"
#include "fake.h"
#include "damage.h"

#define PI 3.14159265f
//#define CFG_DEBUG_CHAT
#define USE_A_KILL
#define TICK_GTE(a,b) (TICK_DIFF(a,b) >= 0)

struct DamageWeapon // also modify the one in damage.h!
{
	Player *p;
	i32 x; // x1000
	i32 y; // x1000
	i16 xspeed;
	i16 yspeed;
	i16 hitx; // When the bomb hits a wall, this is the tile coordinate of the wall
	i16 hity;
	u8 bounce; // bounces left
	u8 proxdistance; // prox distance for this bomb level (in pixels)
	u8 bouncebomb : 1;
	u8 empbomb : 1;
	u8 fuselit : 1; // a ship came within the prox range
	u8 padding : 5;
	u8 removereason; // only used during removal

	ticks_t startedat;
	ticks_t lastupdate;
	ticks_t alivetime;
	struct DamageWeapon *nextbarrel;
	struct Weapons wpn;
	struct DamageProxData prox;
};


struct BotData
{
	Player *p;

	bool manageenergy;

	DamageKilledFunc killfunc;
	DamageRespawnFunc respawnfunc;
	FakeDamageFunc damagefunc;
	void *clos;

	ticks_t lastdied, lastrecharge;

	/* physics system */
	int energy, maxenergy, recharge;
	ticks_t empshutdown_expiresat; //when does the EMP shutdown expire? 0 or < current_ticks() if we have not been EMP'd

	struct C2SPosition *pos;
};

struct async_kill_params
{
	struct BotData *bd;
	Player *killer;
};

struct async_region_params
{
	TileDamageFunc tilefunc;
	Arena *arena;
	Player *firedBy;
	int x, y, damage, wpntype, level, emptime;
	bool bouncingbomb;
	void *clos;
};

struct async_fakedamage_params
{
	FakeDamageFunc func;
	Player *fake;
	Player *firedBy;
        int dist;
	int damage;
	int wtype;
	int level;
        bool bouncing;
	int emptime;
        void *clos;
};

struct RegionData
{
	Region *rgn;
	TileDamageFunc tilefunc;
	void *clos;
};

#define IS_TILE_SOLID(tile) \
	((tile >= 1 && tile <= 161) || \
	(tile >= 192 && tile <= 240) || \
	(tile >= 243 && tile <= 251))

#define WEAPON_LOCK(ad) pthread_mutex_lock(&(ad)->wpnmtx)
#define WEAPON_UNLOCK(ad) pthread_mutex_unlock(&(ad)->wpnmtx)

#define REGION_LOCK(ad) pthread_mutex_lock(&(ad)->rgnmtx)
#define REGION_UNLOCK(ad) pthread_mutex_unlock(&(ad)->rgnmtx)

struct adata
{
	LinkedList wpnset;
	MPQueue wpnset_new; // weapons that have just been added (need to fire callbacks); Entries in this list are also in wpnset
	MPQueue wpnset_old; // weapons that have just been removed (need to fire callbacks + free); Entries in this list are no longer in wpnset
	pthread_mutex_t wpnmtx;

	LinkedList rgnset;
	pthread_mutex_t rgnmtx;

	LinkedList newWeaponCallbacks;
	LinkedList removeWeaponCallbacks;
	LinkedList weaponsUpdatedCallbacks;

	bool IgnoreTeamDamage;
	int  BulletDamageLevel;
	int  BulletDamageUpgrade;
	int  BulletAliveTime;
	bool BulletExactDamage;
	int  BombDamageLevel;
	int  EBombDamagePercent;
	int  BBombDamagePercent;
	int  EBombShutdownTime;
	int  BombAliveTime;
	int  BombExplodePixels;
	int  ProximityDistance;
	int  BombExplodeDelay;
	int  MineAliveTime;

	struct
	{
		int  InitialEnergy;
		int  MaximumEnergy;
		int  MaximumRecharge;
		int  InitialBounty;
		int  BulletSpeed;
		int  BombSpeed;
		int  BombBounceCount;
		bool EmpBomb;
		int  Radius;
		bool DoubleBarrel;
	} ship[8];

};

local void AddFake(Player *p, struct C2SPosition *pos, bool manageenergy,
		DamageKilledFunc killfunc, DamageRespawnFunc respawnfunc, FakeDamageFunc damagefunc, void *clos);
local void KillFake(Player *p, Player *killer);
local void RemoveFake(Player *p);
local void AddRegion(Arena *arena, Region *rgn, TileDamageFunc tilefunc,
			void *clos);
local void RemoveRegion(Arena *arena, Region *rgn);

local void ShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq);
local void PlayerAction(Player *p, int action, Arena *arena);
local void ArenaAction(Arena *arena, int action);

/* use with LLFree */
local void cleanup_turretlist(const void *ptr);

local int respawn_timer(void *bd_);
local int async_kill_timer(void *clos);
local void kill_bot(Arena *arena, ticks_t now, struct BotData *bd, Player *killer);

static long lhypot (register long dx, register long dy);

/* returns true if it bounced */
local int move_vertical(Arena *arena, i32 *x, i32 *y, i16 *xspeed, i16 *yspeed, i16 *hitx, i16 *hity);
local int move_horizontal(Arena *arena, i32 *x, i32 *y, i16 *xspeed, i16 *yspeed, i16 *hitx, i16 *hity);

/* helper functiosn for updating weapons */
local inline int move_thor(Arena *arena, struct DamageWeapon *wd, ticks_t dt);
local inline int move_bouncebullet(Arena *arena, struct DamageWeapon *wd, ticks_t dt);
local inline int move_normal(Arena *arena, struct DamageWeapon *wd, ticks_t dt);
local inline int move_bounce(Arena *arena, struct DamageWeapon *wd, ticks_t dt);

/* returns true if p overlaps the square wx,wy,wr */
local inline int point_collision(int wx, int wy, int wr, Player *p);

/* functions to make the bots take damage */
local int async_fakedamage_timer(void *clos);
local void do_bullet_damage(Arena *arena, struct DamageWeapon *wd, Player *p);
local void do_splash_damage(Arena *arena, struct DamageWeapon *wd);

local int async_region_timer(void *clos);
/* call with the arena region lock held */
local inline void do_region_damage(Arena *arena, struct DamageWeapon *wd);

local struct DamageWeapon* add_weapon(struct adata *ad, Player *p, const struct C2SPosition *pos);
local void remove_weapon(struct adata *ad, struct DamageWeapon *wd, bool removeAllBarrels, Link **_nextLink, u8 removereason);
local void remove_weapons(struct adata *ad, Player *p, u8 removereason);
local void update_weapons(Arena *arena, struct adata *ad, ticks_t now);

local void Pppk(Player *p, const struct C2SPosition *pos);
local void mlfunc(void);


/* list of players we are tracking, uses BotData struct */
local LinkedList turrets;
local pthread_mutex_t turret_mtx = PTHREAD_MUTEX_INITIALIZER;

local int adkey;
local char sintab[40];
local float sintabf[40];

local Imodman *mm;
local Iconfig *cfg;
local Iplayerdata *pd;
local Igame *game;
local Iprng *prng;
local Ichat *chat;
local Iarenaman *aman;
local Imapdata *mapdata;
local Imainloop *ml;
local Ilogman *lm;

local const char *ship_names[8] =
{
	"Warbird",
	"Javelin",
	"Spider",
	"Leviathan",
	"Terrier",
	"Weasel",
	"Lancaster",
	"Shark"
};

local void AddFake(Player *p, struct C2SPosition *pos, bool manageenergy,
	DamageKilledFunc killfunc, DamageRespawnFunc respawnfunc, FakeDamageFunc damagefunc, void *clos)
{
	Arena *arena = p->arena;
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	int ship;
	struct BotData *bd = amalloc(sizeof(*bd));
//	ticks_t now = current_ticks();

	assert(p);
	assert(pos);

	/* handle spec ship */
	if (p->p_ship == SHIP_SPEC)
		ship = 0;
	else
		ship = p->p_ship;

	bd->p = p;

	bd->killfunc = killfunc;
	bd->respawnfunc = respawnfunc;
	bd->damagefunc = damagefunc;
	bd->clos = clos;

	bd->pos = pos;
	bd->lastdied = current_ticks();
	bd->lastrecharge = current_ticks();

	bd->manageenergy = !!manageenergy;

	if (bd->manageenergy)
	{
		/* physics system */
		bd->energy = ad->ship[ship].InitialEnergy;
		bd->maxenergy = ad->ship[ship].MaximumEnergy;
		//bd->energy = bd->maxenergy = bd->energy + (bd->maxenergy - bd->energy) / 2; /* use half way between rand and max */
		//bd->energy = bd->maxenergy = prng->Number(bd->energy, bd->maxenergy); /* use random amount between initial and max */
		bd->energy = bd->maxenergy = bd->energy; /* use initial energy */
		bd->recharge = ad->ship[ship].MaximumRecharge;
		pos->energy = pos->extra.energy = bd->energy;
	}
	/* packet */
	pos->bounty = ad->ship[ship].InitialBounty;

	pthread_mutex_lock(&turret_mtx);
	LLAdd(&turrets, bd);
	pthread_mutex_unlock(&turret_mtx);
}

local void KillFake(Player *p, Player *killer)
{
	//kill_bot(Arena *arena, ticks_t now, struct BotData *bd, Player *killer)
	assert(p->arena && p->arena == killer->arena);

	Link *l;
	pthread_mutex_lock(&turret_mtx);
	for (l = LLGetHead(&turrets); l; l = l->next)
	{
		struct BotData *bd = l->data;
		if (bd->p == p)
		{
			kill_bot(bd->p->arena, current_ticks(), bd, killer);
		}
	}
	pthread_mutex_unlock(&turret_mtx);
}

local void RemoveFake(Player *p)
{
	Link *l;
	pthread_mutex_lock(&turret_mtx);
	for (l = LLGetHead(&turrets); l; l = l->next)
	{
		struct BotData *bd = l->data;
		if (bd->p == p)
		{
			ml->ClearTimer(respawn_timer, bd);
			ml->ClearTimer(async_kill_timer, bd);
			LLRemove(&turrets, bd);
			afree(bd);
			break;
		}
	}
	pthread_mutex_unlock(&turret_mtx);
}

/* use with LLFree */
local void cleanup_turretlist(const void *ptr)
{
    const struct BotData *bd = ptr;
    afree(bd);
}

local int respawn_timer(void *bd_)
{
	struct BotData *bd = bd_;

	//printf("respawn_timer (bd=%08x,p=%08x,ship=%d)\n", bd, bd->p, bd->p->p_ship);

	/* user module may have specced the ship on death */
	if (bd->p->p_ship != SHIP_SPEC)
	{
		/* reset stuff before respawn callback, since it might remove the bot. */
		struct adata *ad = P_ARENA_DATA(bd->p->arena, adkey);
		bd->pos->bounty = ad->ship[bd->p->p_ship].InitialBounty;

		if (bd->manageenergy)
			bd->pos->energy = bd->pos->extra.energy = bd->energy = bd->maxenergy;

		/* fixme: make respawnfunc optional and use arena spawn settings */
		/* this callback is expected to reset the x and y coords */
		bd->respawnfunc(bd->p, bd->clos);
	}

	return 0;
}

local int async_kill_timer(void *clos)
{
	struct async_kill_params *params = clos;
	params->bd->killfunc(params->bd->p, params->killer, params->bd->clos);
	afree(params);
	return 0;
}

local void kill_bot(Arena *arena, ticks_t now, struct BotData *bd, Player *killer)
{
	int notify;
	Player *killed;
	int bty;
	LinkedList advisers = LL_INITIALIZER;
	Akill *killAdviser;
	Link *alink;
	
	/* asss doesn't do the kill callback for fake players, so we make our
	 * own here, may be fixed in future versions, so check this with each
	 * asss release. */
	#if ASSSVERSION_NUM > 0x00010699
	#error check notify_kill in game.c
	#endif
	
	
	notify = 1;
	killed = bd->p;
	bty = bd->pos->bounty;
	
#ifdef USE_A_KILL
	/* Consult the advisers after setting the above flags, the flags reflect the real state of the player */
	mm->GetAdviserList(A_KILL, arena, &advisers);
	FOR_EACH(&advisers, killAdviser, alink)
	{
		if (killAdviser->EditDeath)
		{
			killAdviser->EditDeath(arena, &killer, &killed, &bty);
			
			if (!killed || !killer) // The advisor wants to drop the kill packet
			{
				notify = 0;
				break;
			}
			
			if (killed->status != S_PLAYING || killed->arena != arena)
			{
				lm->LogP(L_ERROR, "damage", killed, "An A_KILL adviser set killed to a bad player");
				mm->ReleaseAdviserList(&advisers);
				return;
			}
			
			if (killer->status != S_PLAYING || killer->arena != arena)
			{
				lm->LogP(L_ERROR, "damage", killed, "An A_KILL adviser set killer to a bad player");
				mm->ReleaseAdviserList(&advisers);
				return;
			}
		}
	}
	mm->ReleaseAdviserList(&advisers);
	
	if (notify)
		game->FakeKill(killer, killed, 0, 0);
#else
	/* kill callback */
	int pts = 0, green = 0;
	DO_CBS(CB_KILL, arena, KillFunc,
		(arena, killer, bd->p, bd->pos->bounty, 0,
		&pts, &green));
	
	game->FakeKill(killer, killed, 0, 0);
	
	/* kill callback */
	DO_CBS(CB_KILL_POST_NOTIFY, arena, KillFunc,
		(arena, killer, bd->p, bd->pos->bounty, 0,
		&pts, &green));

#endif
	
	
	bd->lastdied = now;

	
	/* kill callback */
	DO_CBS(CB_KILL, arena, KillFunc,
		(arena, killer, killed, bty, 0,
		0, 0));


	if (bd->killfunc)
	{
		struct async_kill_params *params = amalloc(sizeof(*params));
		params->bd = bd;
		params->killer = killer;
		ml->SetTimer(async_kill_timer, 0, 0, params, bd);
	}
	bd->pos->x = bd->pos->y = -1;
	bd->pos->xspeed = bd->pos->yspeed = 0;

	/* fixme: get respawn time from settings */
	ml->SetTimer(respawn_timer, 400, 1000, bd, bd);
}

/* quick integer square root */
static long lhypot (register long dx, register long dy)
{
	register unsigned long r, dd;

	dd = dx*dx+dy*dy;

	if (dx < 0) dx = -dx;
	if (dy < 0) dy = -dy;

	/* initial hypotenuse guess (from Gems) */
	r = (dx > dy) ? (dx+(dy>>1)) : (dy+(dx>>1));

	if (r == 0) return (long)r;

	/* converge 3 times */
	r = (dd/r+r)>>1;
	r = (dd/r+r)>>1;
	r = (dd/r+r)>>1;

	return (long)r;
}

local void ShipFreqChange(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
	struct adata *ad = P_ARENA_DATA(p->arena, adkey);

	WEAPON_LOCK(ad);
	remove_weapons(ad, p, DAMAGE_REMOVEWPN_SHIPFREQCHANGE);
	WEAPON_UNLOCK(ad);

	/* see if a tracked player changed ship, so we can reload the max energy
	 * setting and set the current energy to max and set the initial bounty. */
	if (newship != SHIP_SPEC)
	{
		Link *link;
		pthread_mutex_lock(&turret_mtx);
		for (link = LLGetHead(&turrets); link; link = link->next)
		{
			struct BotData *bd = link->data;
			if (bd->p == p)
			{
				bd->pos->bounty = ad->ship[newship].InitialBounty;

				if (bd->manageenergy)
				{
					bd->maxenergy = ad->ship[newship].InitialEnergy;
					bd->pos->energy = bd->pos->extra.energy = bd->energy = bd->maxenergy;
				}
				/* might as well reset these too */
				bd->lastdied = current_ticks();
				bd->lastrecharge = current_ticks();

				break;
			}
		}
		pthread_mutex_unlock(&turret_mtx);
	}
}

local void PlayerAction(Player *p, int action, Arena *arena)
{
	if (action == PA_LEAVEARENA)
	{
		struct adata *ad = P_ARENA_DATA(arena, adkey);

		WEAPON_LOCK(ad);
		remove_weapons(ad, p, DAMAGE_REMOVEWPN_LEAVEARENA);
		WEAPON_UNLOCK(ad);
	}
}

local void ArenaAction(Arena *arena, int action)
{
	struct adata *ad;
	int s;
	struct WeaponData *wd;
	assert(arena);
	assert(adkey != -1);
	ad = P_ARENA_DATA(arena, adkey);
	assert(ad);

	if (action == AA_PRECREATE)
	{
		LLInit(&ad->wpnset);
		MPInit(&ad->wpnset_new);
		MPInit(&ad->wpnset_old);
		pthread_mutex_init(&ad->wpnmtx, NULL);
		LLInit(&ad->rgnset);
		pthread_mutex_init(&ad->rgnmtx, NULL);

		LLInit(&ad->newWeaponCallbacks);
		LLInit(&ad->removeWeaponCallbacks);
		LLInit(&ad->weaponsUpdatedCallbacks);
	}
	else if (action == AA_DESTROY)
	{
		/* lookup bots in this arena and remove them,
		 * cancel their respawn timers. */

		LLEmpty(&ad->newWeaponCallbacks);
		LLEmpty(&ad->removeWeaponCallbacks);
		LLEmpty(&ad->weaponsUpdatedCallbacks);

		Link *l;
		pthread_mutex_lock(&turret_mtx);
		for (l = LLGetHead(&turrets); l; l = l->next)
		{
			struct BotData *bd = l->data;
			if (bd->p->arena == arena)
			{
				ml->ClearTimer(respawn_timer, bd);
				ml->ClearTimer(async_kill_timer, bd);
				LLRemove(&turrets, bd);
				afree(bd);
				break;
			}
		}
		pthread_mutex_unlock(&turret_mtx);

		WEAPON_LOCK(ad);
		LLEnum(&ad->wpnset, afree);
		LLEmpty(&ad->wpnset);
		MPDestroy(&ad->wpnset_new);

		while ( (wd = MPTryRemove(&ad->wpnset_old)) )
			afree(wd);
		MPDestroy(&ad->wpnset_old);

		WEAPON_UNLOCK(ad);

		REGION_LOCK(ad);
		LLEnum(&ad->rgnset, afree);
		LLEmpty(&ad->rgnset);
		REGION_UNLOCK(ad);
	}
	else if (action == AA_POSTDESTROY)
	{
		pthread_mutex_destroy(&ad->wpnmtx);
		pthread_mutex_destroy(&ad->rgnmtx);
	}


	if (action == AA_CREATE || action == AA_CONFCHANGED)
	{
		ConfigHandle ch = arena->cfg;
		ad->IgnoreTeamDamage = cfg->GetInt(ch, "Damage", "IgnoreTeamDamage", 0);
		ad->BulletDamageLevel = cfg->GetInt(ch, "Bullet", "BulletDamageLevel", 200);
		ad->BulletDamageUpgrade = cfg->GetInt(ch, "Bullet", "BulletDamageUpgrade", 100);
		ad->BulletAliveTime = cfg->GetInt(ch, "Bullet", "BulletAliveTime", 550);
		ad->BulletExactDamage = !!cfg->GetInt(ch, "Bullet", "ExactDamage", 0);
		ad->BombDamageLevel = cfg->GetInt(ch, "Bomb", "BombDamageLevel", 750);
		ad->EBombDamagePercent = cfg->GetInt(ch, "Bomb", "EBombDamagePercent", 1000);
		ad->BBombDamagePercent = cfg->GetInt(ch, "Bomb", "BBombDamagePercent", 1000);
		ad->EBombShutdownTime = cfg->GetInt(ch, "Bomb", "EBombShutdownTime", 0);
		ad->BombAliveTime = cfg->GetInt(ch, "Bomb", "BombAliveTime", 8000);
		ad->BombExplodePixels = cfg->GetInt(ch, "Bomb", "BombExplodePixels", 80);
		ad->ProximityDistance = cfg->GetInt(ch, "Bomb", "ProximityDistance", 3);
		ad->BombExplodeDelay = cfg->GetInt(ch, "Bomb", "BombExplodeDelay", 150);
		ad->MineAliveTime = cfg->GetInt(ch, "Mine", "MineAliveTime", 12000);

		for (s = 0; s < 8; s++)
		{
			const char *ship = ship_names[s];
			ad->ship[s].InitialEnergy = cfg->GetInt(ch, ship, "InitialEnergy", 1000);
			ad->ship[s].MaximumEnergy = cfg->GetInt(ch, ship, "MaximumEnergy", 1700);
			ad->ship[s].MaximumRecharge = cfg->GetInt(ch, ship, "MaximumRecharge", 1150);
			ad->ship[s].InitialBounty = cfg->GetInt(ch, ship, "InitialBounty", 50);
			ad->ship[s].BulletSpeed = cfg->GetInt(ch, ship, "BulletSpeed", 2000);
			ad->ship[s].BombSpeed = cfg->GetInt(ch, ship, "BombSpeed", 2000);
			ad->ship[s].BombBounceCount = cfg->GetInt(ch, ship, "BombBounceCount", 0);
			ad->ship[s].EmpBomb = !!cfg->GetInt(ch, ship, "EmpBomb", 0);
			ad->ship[s].Radius =  cfg->GetInt(ch, ship, "Radius", 14);
			ad->ship[s].DoubleBarrel = !!cfg->GetInt(ch, ship, "DoubleBarrel", 0);
		}
	}
}

/* tile damage */

local void AddRegion(Arena *arena, Region *rgn, TileDamageFunc tilefunc,
			void *clos)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	assert(rgn);
	assert(tilefunc);

	if (rgn && tilefunc)
	{
		struct RegionData *d = amalloc(sizeof(*d));

		d->rgn = rgn;
		d->tilefunc = tilefunc;
		d->clos = clos;

		REGION_LOCK(ad);
		LLAdd(&ad->rgnset, d);
		REGION_UNLOCK(ad);
	}
}

local void RemoveRegion(Arena *arena, Region *rgn)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	Link *l;

	REGION_LOCK(ad);
	for (l = LLGetHead(&ad->rgnset); l; l = l->next)
	{
		struct RegionData *rd = l->data;
		if (rd->rgn == rgn)
		{
			LLRemove(&ad->rgnset, rd);
			afree(rd);
			break;
		}
	}
	REGION_UNLOCK(ad);
}

local int async_region_timer(void *clos)
{
	struct async_region_params *d = clos;
    d->tilefunc(d->arena, d->x, d->y, d->firedBy, d->damage, d->wpntype,
                d->level, d->bouncingbomb, d->emptime, d->clos);
	afree(d);
	return 0;
}

/* call with the arena region lock held */
local void do_region_damage(Arena *arena, struct DamageWeapon *wd)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	Link *l;
	int x = wd->x / 16000;
	int y = wd->y / 16000;

	for (l = LLGetHead(&ad->rgnset); l; l = l->next)
	{
		struct RegionData *rd = l->data;
		/* assuming regions don't overlap, otherwise the damage calculation
		 * should be moved outside the loop or at least cached after the first
		 * time it is calculated. */
		if (mapdata->Contains(rd->rgn, x, y))
		{
                        int shutdownTime = 0;
			int wtype = wd->wpn.type;
			u32 damage = 0;

			if (wtype == W_BULLET)
			{
				u32 maxdamage = ad->BulletDamageLevel +	ad->BulletDamageUpgrade * wd->wpn.level;

				/* avoid divide by zero error and wasting time */
				if (maxdamage < 1)
					continue;

				if (ad->BulletExactDamage)
					damage = maxdamage;
				else
					/* http://www.shanky.com/server/faq/server-cfg.html#damage
					 * says: Hit the random number
					 * http://www.shanky.com/server/faq/misc.html#1064680230
					 * says: square root(rand# * (maxdamage^2 + 1)) */
					damage = prng->Rand() % maxdamage;
			}
			else if (wtype == W_BOMB || wtype == W_PROXBOMB)
			{
                		damage = ad->BombDamageLevel;

 		                if (wd->empbomb)
				   damage = damage * ad->EBombDamagePercent / 1000;

				if (wd->bouncebomb)
				    damage = damage * ad->BBombDamagePercent / 1000;

				if (wd->empbomb && damage) // continuum does not do emp if bomb damage is 0
				{
					shutdownTime = ad->EBombShutdownTime;
				}
			}

			if (damage && rd->tilefunc)
			{
				struct async_region_params *d = amalloc(sizeof(*d));
				d->tilefunc = rd->tilefunc;
				d->arena = arena;
				d->x = x;
				d->y = y;
				d->firedBy = wd->p;
				d->damage = damage;
				d->wpntype = wd->wpn.type;
				d->level = wd->wpn.level;
				d->emptime = shutdownTime;
				d->bouncingbomb = !!wd->bouncebomb;
				d->clos = rd->clos;

				ml->SetTimer(async_region_timer, 0, 0, d, NULL);
			}
		}
	}
}


/* map stuff */

/* returns true if it bounced */
local int move_vertical(Arena *arena, i32 *x, i32 *y, i16 *xspeed, i16 *yspeed, i16 *hitx, i16 *hity)
{
	int bounced = 0;
	int ny = *y + *yspeed;
	int tx = (*x / 1000) >> 4;
	int ty = (ny / 1000) >> 4;
	enum map_tile_t tile = mapdata->GetTile(arena, tx, ty);

	/* check for collision */
	if (IS_TILE_SOLID(tile))
	{
		*hitx = tx;
		*hity = ty;
		/* align */
		if (*yspeed >= 0)
			*y = (ty << 4) * 1000 - 1;
		else
			*y = ((ty + 1) << 4) * 1000;

		*yspeed = - *yspeed;
		bounced = 1;
	}
	else
	{
		*y = ny;
	}

	return bounced;
}

/* returns true if it bounced */
local int move_horizontal(Arena *arena, i32 *x, i32 *y, i16 *xspeed, i16 *yspeed, i16 *hitx, i16 *hity)
{
	int bounced = 0;
	int nx = *x + *xspeed;
	int tx = (nx / 1000) >> 4;
	int ty = (*y / 1000) >> 4;
	enum map_tile_t tile = mapdata->GetTile(arena, tx, ty);

	/* check for collision */
	if (IS_TILE_SOLID(tile))
	{
		*hitx = tx;
		*hity = ty;
		/* align */
		if (*xspeed >= 0)
			*x = (tx << 4) * 1000 - 1;
		else
			*x = ((tx + 1) << 4) * 1000;

		*xspeed = - *xspeed;
		bounced = 1;
	}
	else
	{
		*x = nx;
	}

	return bounced;
}

/* damage system */

/* call with lock */
local struct DamageWeapon* add_weapon(struct adata *ad, Player *p, const struct C2SPosition *pos)
{
	struct DamageWeapon *wd;
	int ship = p->p_ship;
	int shipradius;
	int speed;
	float offset1, offset2;

	shipradius = ad->ship[ship].Radius;

	if (pos->weapon.type == W_PROXBOMB || pos->weapon.type == W_THOR)
		wd = amalloc(sizeof(struct DamageWeapon));
	else
		wd = amalloc(sizeof(struct DamageWeapon) - sizeof(struct DamageProxData));

	wd->p = p;
	wd->x = pos->x * 1000;
	wd->y = pos->y * 1000;
	wd->xspeed = pos->xspeed;
	wd->yspeed = pos->yspeed;
	/* lag the weapon on purpose, otherwise a bomb that kills the bot will
	 * fly straight through. fixme: recheck when prox is fixed. */
	wd->startedat = wd->lastupdate = current_ticks();
	/* wd->startedat = wd->lastupdate = pos->time; */
	wd->wpn = pos->weapon;
	wd->nextbarrel = NULL;

	if (pos->weapon.type == W_BULLET || pos->weapon.type == W_BOUNCEBULLET)
	{
		/* not bothering with multifire */
		speed = ad->ship[ship].BulletSpeed;
		wd->xspeed += (speed * sintab[pos->rotation]) >> 7;
		wd->yspeed += (speed * sintab[(pos->rotation + 30) % 40]) >> 7;

		wd->alivetime = TICK_MAKE(ad->BulletAliveTime);

		if (pos->weapon.type == W_BOUNCEBULLET)
			wd->bounce = 1;

		if (ad->ship[ship].DoubleBarrel)
		{
			wd->nextbarrel = amalloc(sizeof(struct DamageWeapon) - sizeof(struct DamageProxData));
			memcpy(wd->nextbarrel, wd, sizeof(struct DamageWeapon) - sizeof(struct DamageProxData));
			wd->nextbarrel->nextbarrel = wd;

			offset2 = shipradius * 0.7;
			if (shipradius == 14) // special case, probably for VIE compatability
				offset1 = offset2;
			else
				offset1 = offset2 + 1;

			// double barrel 1(left one with rot = 0)
			wd->x += (int)round(offset1 * sintabf[(pos->rotation + 30) % 40] * 1000);
			wd->y -= (int)round(offset1 * sintabf[pos->rotation] * 1000);

			// double barrel 2 (right one with rot = 0)
			wd->nextbarrel->x -= (int)round(offset2 * sintabf[(pos->rotation + 30) % 40] * 1000);
			wd->nextbarrel->y += (int)round(offset2 * sintabf[pos->rotation] * 1000);

#ifdef CFG_DEBUG_CHAT
			chat->SendArenaMessage(p->arena, "%s> Fired double barrel @ (%d; %d): (%d; %d; %d; %d) and (%d; %d; %d; %d)",
			                                 p->name,
			                                 pos->x, pos->y,
			                                 wd->x/1000, wd->y/1000, wd->xspeed, wd->yspeed,
			                                 wd->nextbarrel->x/1000, wd->nextbarrel->y/1000, wd->nextbarrel->xspeed, wd->nextbarrel->yspeed
			                                 );
#endif
		}
	}
	else if (pos->weapon.type == W_BOMB || pos->weapon.type == W_PROXBOMB ||
		pos->weapon.type == W_THOR)
	{
		if (pos->weapon.alternate == 0)
		{
			speed = ad->ship[ship].BombSpeed;
			wd->xspeed += (speed * sintab[pos->rotation]) >> 7;
			wd->yspeed += (speed * sintab[(pos->rotation + 30) % 40]) >> 7;
			wd->alivetime = TICK_MAKE(ad->BombAliveTime);
			if (pos->weapon.type == W_BOMB || pos->weapon.type == W_PROXBOMB)
			{
				wd->bounce = ad->ship[ship].BombBounceCount;
				wd->bouncebomb = !!wd->bounce;
				wd->empbomb = ad->ship[ship].EmpBomb;
			}
			if (pos->weapon.type == W_PROXBOMB || pos->weapon.type == W_THOR)
			{
				int proxdistance;
				proxdistance = ad->ProximityDistance;
				proxdistance += pos->weapon.level;
				if (pos->weapon.type == W_THOR)
					proxdistance += 3;

				proxdistance = proxdistance * 16;
				CLIP(proxdistance, 0, 255); // ProximityDistance > 12
				wd->proxdistance = proxdistance;

				wd->prox.delay = ad->BombExplodeDelay;
				wd->prox.pid = -1; /* make this unset/no one */
//				chat->SendArenaMessage(p->arena, "prox bomb detected (wr=%d)",
//					wd->proxdistance);
			}
		}
		else
		{
			/* mine */
			wd->xspeed = 0;
			wd->yspeed = 0;
			wd->alivetime = TICK_MAKE(ad->MineAliveTime);
		}
	}
	else
	{
		/* only interested in guns/bombs at the moment */
		afree(wd);
		return NULL;
	}

	LLAdd(&ad->wpnset, wd);
	MPAdd(&ad->wpnset_new, wd);
	if (wd->nextbarrel)
	{
		LLAdd(&ad->wpnset, wd->nextbarrel);
		MPAdd(&ad->wpnset_new, wd->nextbarrel);
	}
	return wd;
}

/* call with weapon lock*/
local void remove_weapon(struct adata *ad, struct DamageWeapon *wd, bool removeAllBarrels, Link **_nextLink, u8 removereason)
{
	struct DamageWeapon *wd2, *next;
	Link *nextLink = NULL;

	if (_nextLink)
		nextLink = *_nextLink;

	if (removeAllBarrels)
	{
		wd2 = wd->nextbarrel;

		while (wd2 &&
		       wd2 != wd) // do not clear wd, this is done below
		{
			next = wd2->nextbarrel;

			if (nextLink && nextLink->data == wd2)
				nextLink = nextLink->next;

			LLRemove(&ad->wpnset, wd2);
			if (removereason == DAMAGE_REMOVEWPN_HITPLAYER)
				wd2->removereason = DAMAGE_REMOVEWPN_OTHERBARRELHIT;
			else
				wd2->removereason = removereason;
			MPAdd(&ad->wpnset_old, wd2);
			wd2 = next;
		}
	}
	else
	{
		if (wd->nextbarrel)
		{
			wd2 = wd;
			while (wd2->nextbarrel != wd)
			{
				wd2 = wd2->nextbarrel;
				assert(wd2);
			}

			// we now have the barrel before this one
			wd2->nextbarrel = wd->nextbarrel;

			if (wd2->nextbarrel == wd2) // last remaining barrel
				wd2->nextbarrel = NULL;
		}
	}

	if (nextLink && nextLink->data == wd)
		nextLink = nextLink->next;
	LLRemove(&ad->wpnset, wd);
	wd->removereason = removereason;
	MPAdd(&ad->wpnset_old, wd);

	if (_nextLink)
		*_nextLink = nextLink;
}

/* call with lock */
local void remove_weapons(struct adata *ad, Player *p, u8 removereason)
{
	Link *l, *next;
	for (l = LLGetHead(&ad->wpnset); l; l = next)
	{
		struct DamageWeapon *wd = l->data;
		next = l->next;
		if (wd->p == p)
		{
			remove_weapon(ad, wd, true, next ? &next : NULL, removereason);
		}
#if deprecated
		else if (wd->fuselit && wd->prox.who == p)
		{
			/* cancel prox by distance */
			wd->prox.who = NULL;
		}
#endif
	}
}

/* returns true if it should be removed */
local inline int move_thor(Arena *arena, struct DamageWeapon *wd, ticks_t dt)
{
	wd->x += wd->xspeed * dt;
	wd->y += wd->yspeed * dt;
	return 0;
}

/* returns true if it should be removed */
local inline int move_bouncebullet(Arena *arena, struct DamageWeapon *wd, ticks_t dt)
{
	while(dt-- > 0)
	{
		move_vertical(arena, &wd->x, &wd->y, &wd->xspeed, &wd->yspeed, &wd->hitx, &wd->hity);
		move_horizontal(arena, &wd->x, &wd->y, &wd->xspeed, &wd->yspeed, &wd->hitx, &wd->hity);
	}
	return 0;
}

/* returns true if it should be removed */
local inline int move_normal(Arena *arena, struct DamageWeapon *wd, ticks_t dt)
{
	int die = 0;
	while(dt-- > 0 && !die)
	{
		die += move_vertical(arena, &wd->x, &wd->y, &wd->xspeed, &wd->yspeed, &wd->hitx, &wd->hity);
		die += move_horizontal(arena, &wd->x, &wd->y, &wd->xspeed, &wd->yspeed, &wd->hitx, &wd->hity);
	}
	return die;
}

/* returns true if it should be removed. adjusts wd->bounce for you */
local inline int move_bounce(Arena *arena, struct DamageWeapon *wd, ticks_t dt)
{
	int die = 0;
	while(dt-- > 0 && !die)
	{
		wd->bounce -= move_vertical(arena, &wd->x, &wd->y, &wd->xspeed, &wd->yspeed, &wd->hitx, &wd->hity);
		if (wd->bounce == 0)
		    return move_normal(arena, wd, dt);

		wd->bounce -= move_horizontal(arena, &wd->x, &wd->y, &wd->xspeed, &wd->yspeed, &wd->hitx, &wd->hity);
		if (wd->bounce == 0)
		    return move_normal(arena, wd, dt);
	}
	return die;
}

/* returns true if p overlaps the square wx,wy,wr */
local inline int point_collision(int wx, int wy, int wr, Player *p)
{
	struct adata *ad = P_ARENA_DATA(p->arena, adkey);
	int radius = 0;
	/* take ship radius into account with non-prox weapons */
	if (wr <= 8)
	{
		radius = ad->ship[p->p_ship].Radius;
		if (!radius) radius = 14;
	}

/* adjust for 5 ticks, 5 = half spd (usually) */
#if adjust2
#define XADJUST (p->position.xspeed * 5 / 1000)
#define YADJUST (p->position.yspeed * 5 / 1000)
#else
#define XADJUST (0)
#define YADJUST (0)
#endif

	if (p->position.x + XADJUST + radius < wx - wr) return FALSE;
	if (p->position.x + XADJUST - radius > wx + wr) return FALSE;
	if (p->position.y + YADJUST + radius < wy - wr) return FALSE;
	if (p->position.y + YADJUST - radius > wy + wr) return FALSE;

	/* prox uses circular collision */
	//if (wr > 8)
	//	if (lhypot(p->position.x - wx, p->position.y - wy) > wr)
	//	    return FALSE;

//	chat->SendArenaMessage(p->arena, "collision (%d,%d) with (%d,%d) (r=%d)",
//		wx, wy, p->position.x, p->position.y, radius);

	return TRUE;
}

#if 0
local inline int prox_collision(int wx, int wy, int wr, Player *p)
{
	if (p->position.x / 16 < (wx - wr) / 16) return FALSE;
	if (p->position.x / 16 > (wx + wr) / 16) return FALSE;
	if (p->position.y / 16 < (wy - wr) / 16) return FALSE;
	if (p->position.y / 16 > (wy + wr) / 16) return FALSE;

	/* circular collision */
	if (lhypot(p->position.x / 16 - wx / 16, p->position.y / 16 - wy / 16) > wr / 16)
	    return FALSE;

	return TRUE;
}
#endif

local int async_fakedamage_timer(void *clos)
{
	struct async_fakedamage_params *d = clos;
	d->func(d->fake, d->firedBy, d->dist, d->damage, d->wtype, d->level, d->bouncing, d->emptime, d->clos);
	afree(d);
	return 0;
}

local void do_bullet_damage(Arena *arena, struct DamageWeapon *wd, Player *p)
{
	Link *link;
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	u32 damage, maxdamage = ad->BulletDamageLevel + ad->BulletDamageUpgrade	* wd->wpn.level;
	ticks_t now = current_ticks();

	/* avoid divide by zero error and wasting time */
	if (maxdamage < 1)
	{
		return;
	}

	if (ad->BulletExactDamage)
		damage = maxdamage;
	else
		/* http://www.shanky.com/server/faq/server-cfg.html#damage
		 * says: Hit the random number
		 * http://www.shanky.com/server/faq/misc.html#1064680230
		 * says: square root(rand# * (maxdamage^2 + 1)) */
		damage = prng->Rand() % maxdamage;

	pthread_mutex_lock(&turret_mtx);
	for (link = LLGetHead(&turrets); link; link = link->next)
	{
		struct BotData *bd = link->data;
		if (bd->p == p && TICK_DIFF(now, bd->lastdied) > 150)
		{
			if (bd->manageenergy)
			{
				bd->energy -= damage;
				bd->pos->energy = bd->pos->extra.energy = bd->energy;
				if (bd->energy < 0)
					kill_bot(arena, now, bd, wd->p);
			}

			if (bd->damagefunc)
			{
				struct async_fakedamage_params *params = amalloc(sizeof(*params));
				params->func = bd->damagefunc;
				params->fake = bd->p;
				params->firedBy = wd->p;
				params->dist = 0;
				params->damage = damage;
				params->wtype = wd->wpn.type;
				params->level = wd->wpn.level;
				params->bouncing = wd->wpn.type == W_BOUNCEBULLET;
				params->emptime = 0;
				params->clos = bd->clos;
				ml->SetTimer(async_fakedamage_timer, 0, 0, params, NULL);
			}

#ifdef CFG_DEBUG_CHAT
			chat->SendArenaMessage(arena, "%s> ow! (%d bullet damage) %d", bd->p->name, damage, !!bd->damagefunc);
#endif
			break;
		}
	}
	pthread_mutex_unlock(&turret_mtx);
}

local void do_splash_damage(Arena *arena, struct DamageWeapon *wd)
{
	Link *link;
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	int x = wd->x / 1000;
	int y = wd->y / 1000;
	int cfg_ignoreteamdamage = ad->IgnoreTeamDamage;
	int radius = ad->BombExplodePixels;
	u32 damage, maxdamage = ad->BombDamageLevel;
	ticks_t now = current_ticks();

	/* adjust radius according to bomb level (thor being L4) */
	radius = radius * (wd->wpn.level + (wd->wpn.type == W_THOR ? 4 : 1));

	/* avoid divide by zero error and wasting time */
	if (radius < 1 || maxdamage < 1)
		return;

	if (wd->empbomb)
	    maxdamage = maxdamage * ad->EBombDamagePercent / 1000;

	if (wd->bouncebomb)
	    maxdamage = maxdamage * ad->BBombDamagePercent / 1000;

	pthread_mutex_lock(&turret_mtx);
	for (link = LLGetHead(&turrets); link; link = link->next)
	{
		struct BotData *bd = link->data;
		if (bd->p->arena == arena &&
			bd->p->p_ship != SHIP_SPEC &&
			TICK_DIFF(now, bd->lastdied) > 150)
		{
#if adjust1
			int bx = bd->pos->x + (bd->pos->xspeed * (now - bd->pos->time) / 1000);
			int by = bd->pos->y + (bd->pos->yspeed * (now - bd->pos->time) / 1000);
#else
			int bx = bd->pos->x;
			int by = bd->pos->y;
#endif
			int shutdownTime = 0;
                        bool ignoreDamage = !bd->manageenergy || (cfg_ignoreteamdamage && bd->p->p_freq == wd->p->p_freq);

			int dist = lhypot(x - bx, y - by);
			if (dist < radius)
			{
				/*
				damage = maxdamage * (radius - dist) / radius;
				*/
				damage = dist * - (i32)maxdamage / radius + maxdamage;
				if (!ignoreDamage)
				{
                			bd->energy -= damage;
            				bd->pos->energy = bd->pos->extra.energy = bd->energy;
                                }

				if (wd->empbomb && maxdamage) // continuum does not do emp if bomb damage is 0
				{
					int EBombShutDownTime = ad->EBombShutdownTime;
					shutdownTime = dist * -EBombShutDownTime / radius + EBombShutDownTime;


					if (TICK_GT(now + shutdownTime, bd->empshutdown_expiresat))
						bd->empshutdown_expiresat = TICK_MAKE(now + shutdownTime);
				}

				if (bd->energy < 0 && !ignoreDamage)
				{
					/* prevent suicides */
					if (bd->p != wd->p)
						kill_bot(arena, now, bd, wd->p);
					else
					{
						bd->energy = 0;
						bd->pos->energy = bd->pos->extra.energy = 0;
					}
				}

				if (bd->damagefunc)
				{
					struct async_fakedamage_params *params = amalloc(sizeof(*params));
					params->func = bd->damagefunc;
					params->fake = bd->p;
					params->firedBy = wd->p;
					params->dist = dist;
					params->damage = damage;
					params->wtype = wd->wpn.type;
					params->level = wd->wpn.level;
					params->bouncing = !!wd->bouncebomb;
					params->emptime = shutdownTime;
					params->clos = bd->clos;
					ml->SetTimer(async_fakedamage_timer, 0, 0, params, NULL);
				}

#ifdef CFG_DEBUG_CHAT
				chat->SendArenaMessage(arena, "%s> ow! (%d bomb damage) (%d,%d,%d)",
					bd->p->name, damage, dist, maxdamage, radius);
#endif
			}
		}
	}
	pthread_mutex_unlock(&turret_mtx);
}


/* call with weapon lock */
local void update_weapons(Arena *arena, struct adata *ad, ticks_t now)
{
	Link *l, *next;

	for (l = LLGetHead(&ad->wpnset); l; l = next)
	{
		struct DamageWeapon *wd = l->data;
		next = l->next;
		if (TICK_GTE(TICK_DIFF(now, wd->startedat), wd->alivetime))
		{
			remove_weapon(ad, wd, true, next ? &next : NULL, DAMAGE_REMOVEWPN_EXPIRED);
		}
		else if (TICK_DIFF(now, wd->lastupdate) > 0)
		{
			ticks_t dt = TICK_DIFF(now, wd->lastupdate);
			u8 remove = 0;
			bool removeAllBarrels = 0;

			if (wd->wpn.type == W_BOUNCEBULLET)
				move_bouncebullet(arena, wd, dt);
			else if (wd->wpn.type == W_BULLET)
			{
				if (move_normal(arena, wd, dt))
					remove = DAMAGE_REMOVEWPN_HIT;

				if (remove)
					do_region_damage(arena, wd);
			}
			else if (wd->wpn.type == W_BOMB || wd->wpn.type == W_PROXBOMB)
			{
				if (wd->bounce == 0)
				{
					if (move_normal(arena, wd, dt))
						remove = DAMAGE_REMOVEWPN_HIT;
				}
				else
				{
					if (move_bounce(arena, wd, dt))
						remove = DAMAGE_REMOVEWPN_HIT;
				}

				if (remove)
				{
					do_splash_damage(arena, wd);
					do_region_damage(arena, wd);
				}
			}
			else if (wd->wpn.type == W_THOR)
			    move_thor(arena, wd, dt);

			/* bomb timer stuff */
			if (wd->fuselit == 1)
			{
				/* check distance between the bomb and the
				 * player that triggered it, also check to see
				 * if the fuse has run out. */
				Player *p = pd->PidToPlayer(wd->prox.pid);
				int dist;
				if (p)
				{
					/* distance between triggerer and bomb */
					dist = lhypot(
						p->position.x - wd->x / 1000,
						p->position.y - wd->y / 1000);
					/* reduce resolution to 1 tile */
//					dist = (dist / 16) * 16;
				}
				else
				{
					/* triggerer left arena, set dist really
					 * large to force it to explode. */
					dist = 10000;
				}

				/* if the distance increases or the fuse burns out then explode
				 * note: if the distance is 0 it can only increase */
				if (dist == 0 || dist > wd->prox.dist || TICK_DIFF(now, wd->prox.triggered) >= wd->prox.delay)
				{
#ifdef CFG_DEBUG_CHAT
					chat->SendArenaMessage(arena, "prox bomb exploding (fuseleft=%d,dist=%d>=%d)",
						wd->prox.delay - (TICK_DIFF(now, wd->prox.triggered)), dist, wd->prox.dist);
#endif
					do_splash_damage(arena, wd);
					wd->alivetime = 0;
					remove = DAMAGE_REMOVEWPN_HITPLAYER;
					removeAllBarrels = true; // if a bullet hits a player, all its barrels are removed so that only 1 bullet deals damage
				}
				else
				{
					/* otherwise it is moving closer or parallel, update dist */
#ifdef CFG_DEBUG_CHAT
					//if (dist != wd->prox.dist)
						chat->SendArenaMessage(arena, "prox bomb ticking (fuseleft=%d,dist=%d)",
							wd->prox.delay - (TICK_DIFF(now, wd->prox.triggered)), dist);
#endif
					wd->prox.dist = dist;
					wd->lastupdate = now;
				}
			}

			if (!remove && !wd->fuselit)
			{
				/* check for collisions with enemy ships */
				Link *link;
				Player *p;
				int wx = wd->x / 1000;
				int wy = wd->y / 1000;
				int wr;

				/* something funny with prox radius, maybe just cont messing up. */
				if (wd->wpn.type == W_PROXBOMB || wd->wpn.type == W_THOR)
				    wr = wd->proxdistance;
				else if (wd->wpn.type == W_BOMB)
				    wr = 7;
				else
				    wr = 3; /* bullet radius! */

				pd->Lock();
				FOR_EACH_PLAYER(p)
				{
					if (p->arena == arena && p->p_ship != SHIP_SPEC
						&& p->p_freq != wd->p->p_freq &&
						!p->flags.is_dead)
					{
						if (point_collision(wx, wy, wr, p))
						{
							/* prox triggering */
							if (wd->wpn.type == W_PROXBOMB || wd->wpn.type == W_THOR)
							{
								wd->fuselit = 1;
								wd->prox.triggered = now;
								wd->prox.pid = p->pid;
								//wd->prox.x = p->position.x;
								//wd->prox.y = p->position.y;
								wd->prox.dist = lhypot(p->position.x - wx, p->position.y - wy);
								/* reduce resolution to 1 tile */
//								wd->prox.dist = (wd->prox.dist / 16) * 16;
#ifdef CFG_DEBUG_CHAT
								chat->SendArenaMessage(arena, "prox bomb triggered (%d<=%d)",
									wd->prox.dist, wd->proxdistance);
#endif
								break;
							}
							else
							{
								if (wd->wpn.type == W_BOMB)
									do_splash_damage(arena, wd);
								/* not a bomb, do bullet damage */
								else if (p->type == T_FAKE)
									do_bullet_damage(arena, wd, p);
								wd->alivetime = 0;
								remove = DAMAGE_REMOVEWPN_HITPLAYER;
								removeAllBarrels = true;
								break;
							}
						}
					}
				}
				pd->Unlock();

				wd->lastupdate = now;
			}

			if (remove)
			{
				remove_weapon(ad, wd, removeAllBarrels, next ? &next : NULL, remove);
			}
		}
	}
}

local void Pppk(Player *p, const struct C2SPosition *pos)
{
	Arena *arena = p->arena;

	if (arena && p->status == S_PLAYING &&
		p->p_ship >= SHIP_WARBIRD && p->p_ship <= SHIP_SHARK &&
		pos->x != -1 && pos->y != -1)
	{
		struct adata *ad = P_ARENA_DATA(arena, adkey);

		if (pos->weapon.type)
		{
			WEAPON_LOCK(ad);
			add_weapon(ad, p, pos);
			WEAPON_UNLOCK(ad);
		}
		else if (pos->status & STATUS_SAFEZONE)
		{
			/* could optimise here by storing last status */
			WEAPON_LOCK(ad);
			remove_weapons(ad, p, DAMAGE_REMOVEWPN_SAFEZONE);
			WEAPON_UNLOCK(ad);
		}
	}
}

// do not call below 3 with weapon lock
local void DoNewWeaponCB(Arena *arena, DamageWeapon *wpn)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	Link *l;
	DamageNewWeaponFunc fn;

	for (l = LLGetHead(&ad->newWeaponCallbacks); l; l = l->next)
	{
		fn = l->data;
		fn(arena, wpn);
	}
}

local void DoRemoveWeaponCB(Arena *arena, DamageWeapon *wpn, u8 removereason)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	Link *l;
	DamageRemoveWeaponFunc fn;

	for (l = LLGetHead(&ad->removeWeaponCallbacks); l; l = l->next)
	{
		fn = l->data;
		fn(arena, wpn, removereason);
	}
}

local void DoWeaponsUpdatedCB(Arena *arena, ticks_t now)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	Link *l;
	DamageWeaponsUpdatedFunc fn;

	for (l = LLGetHead(&ad->weaponsUpdatedCallbacks); l; l = l->next)
	{
		fn = l->data;
		fn(arena, now);
	}
}

local void mlfunc()
{
	Link *link;
	Arena *arena;
	struct adata *ad;
	ticks_t now = current_ticks();
	DamageWeapon *wd;

	aman->Lock();
	FOR_EACH_ARENA_P(arena, ad, adkey)
	{
		if (arena->status == ARENA_RUNNING)
		{
			while ( (wd = MPTryRemove(&ad->wpnset_new)) )
			{
				DoNewWeaponCB(arena, wd);
			}

			WEAPON_LOCK(ad);
			REGION_LOCK(ad);
			update_weapons(arena, ad, now);
			REGION_UNLOCK(ad);
			WEAPON_UNLOCK(ad);

			while ( (wd = MPTryRemove(&ad->wpnset_old)) )
			{
				DoRemoveWeaponCB(arena, wd, wd->removereason);
				afree(wd);
			}

			DoWeaponsUpdatedCB(arena, now);

		}
	}
	aman->Unlock();

	pthread_mutex_lock(&turret_mtx);
	for (link = LLGetHead(&turrets); link; link = link->next)
	{
		struct BotData *bd = link->data;

		if (bd->manageenergy)
		{
			/* energy stuff */
			if (bd->energy < bd->maxenergy)
			{
				int diff = (bd->recharge * (TICK_DIFF(now, bd->lastrecharge))) / 1000;
				if (diff)
				{
					if (TICK_GT(now, bd->empshutdown_expiresat)) // not emped?
						bd->energy += diff;

					if (bd->energy > bd->maxenergy)
						bd->energy = bd->maxenergy;
					bd->lastrecharge = now;

					bd->pos->energy = bd->pos->extra.energy = bd->energy;
				}
			}
			else
			    bd->lastrecharge = now;
		}
	}
	pthread_mutex_unlock(&turret_mtx);
}



local void AddWeaponCallback(Arena *arena, DamageNewWeaponFunc addFunc, DamageRemoveWeaponFunc removeFunc, DamageWeaponsUpdatedFunc updatedFunc)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);

	if (addFunc)
		LLAdd(&ad->newWeaponCallbacks, addFunc);

	if (removeFunc)
		LLAdd(&ad->removeWeaponCallbacks, removeFunc);

	if (updatedFunc)
		LLAdd(&ad->weaponsUpdatedCallbacks, updatedFunc);
}

local void  RemoveWeaponCallback(Arena *arena, DamageNewWeaponFunc addFunc, DamageRemoveWeaponFunc removeFunc, DamageWeaponsUpdatedFunc updatedFunc)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);

	if (addFunc)
		LLRemove(&ad->newWeaponCallbacks, addFunc);

	if (removeFunc)
		LLRemove(&ad->removeWeaponCallbacks, removeFunc);

	if (updatedFunc)
		LLRemove(&ad->weaponsUpdatedCallbacks, updatedFunc);
}

local void WeaponLock(Arena *arena)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	WEAPON_LOCK(ad);
}

local void  WeaponUnlock(Arena *arena)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	WEAPON_UNLOCK(ad);
}

local Link* LLGetHeadWeapons(Arena *arena)
{
	struct adata *ad = P_ARENA_DATA(arena, adkey);
	return LLGetHead(&ad->wpnset);
}

local Idamage _myint =
{
	INTERFACE_HEAD_INIT(I_DAMAGE, "damage")

	AddFake, KillFake, RemoveFake,
	AddRegion, RemoveRegion,

	AddWeaponCallback, RemoveWeaponCallback,
	WeaponLock, WeaponUnlock, LLGetHeadWeapons
};

EXPORT const char info_damage[] =
	"damage v1.26\n"
        "smong <soinsg@hotmail.com>\n"
	"based on bot1 v1.7w smong <soinsg@hotmail.com>\n"
	"some additions by JoWie <jowie@welcome-to-the-machine.com>";

EXPORT int MM_damage(int action, Imodman *mm_, Arena *arena)
{
	if (action == MM_LOAD)
	{
		mm = mm_;
		pd = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
		cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
		game = mm->GetInterface(I_GAME, ALLARENAS);
		prng = mm->GetInterface(I_PRNG, ALLARENAS);
		chat = mm->GetInterface(I_CHAT, ALLARENAS);
		aman = mm->GetInterface(I_ARENAMAN, ALLARENAS);
		mapdata = mm->GetInterface(I_MAPDATA, ALLARENAS);
		ml = mm->GetInterface(I_MAINLOOP, ALLARENAS);
		lm = mm->GetInterface(I_LOGMAN, ALLARENAS);
		if (!pd || !cfg || !game || !prng || !chat ||
			!aman || !mapdata || !ml || !lm)
			return MM_FAIL;

		adkey = aman->AllocateArenaData(sizeof(struct adata));
		if (adkey == -1) return MM_FAIL;

		LLInit(&turrets);
		mm->RegCallback(CB_MAINLOOP, mlfunc, ALLARENAS);
		mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChange, ALLARENAS);
		mm->RegCallback(CB_PLAYERACTION, PlayerAction, ALLARENAS);
		mm->RegCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);
		mm->RegCallback(CB_PPK, Pppk, ALLARENAS);
		mm->RegInterface(&_myint, ALLARENAS);

		/* generate custom sine table */
		{
			int i;
			for (i = 0; i < 40; i++)
			{
				double s = sin(i * PI / 20);
				sintabf[i] = s;
	            		sintab[i] = (1 << 7) * s;
	            	}
		}

		/* if loaded after arenas have been created */
		{
			Arena *a;
			Link *link;
			aman->Lock();
			FOR_EACH_ARENA(a)
			{
				ArenaAction(a, AA_PRECREATE);
				ArenaAction(a, AA_CREATE);
			}
			aman->Unlock();
		}
		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		if (mm->UnregInterface(&_myint, ALLARENAS))
			return MM_FAIL;

		/* if unloaded while arenas are still running */
		{
			Arena *a;
			Link *link;
			aman->Lock();
			FOR_EACH_ARENA(a)
			{
				ArenaAction(a, AA_DESTROY);
				ArenaAction(a, AA_POSTDESTROY);
			}
			aman->Unlock();
		}

		mm->UnregCallback(CB_MAINLOOP, mlfunc, ALLARENAS);
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChange, ALLARENAS);
		mm->UnregCallback(CB_PLAYERACTION, PlayerAction, ALLARENAS);
		mm->UnregCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);
		mm->UnregCallback(CB_PPK, Pppk, ALLARENAS);
		ml->ClearTimer(respawn_timer, NULL);
		ml->ClearTimer(async_kill_timer, NULL);
		/* if unloaded while bots are still flying */
		LLEnum(&turrets, cleanup_turretlist);
		LLEmpty(&turrets);
		aman->FreeArenaData(adkey);
		mm->ReleaseInterface(pd);
		mm->ReleaseInterface(cfg);
		mm->ReleaseInterface(game);
		mm->ReleaseInterface(prng);
		mm->ReleaseInterface(chat);
		mm->ReleaseInterface(aman);
		mm->ReleaseInterface(mapdata);
		mm->ReleaseInterface(ml);
		mm->ReleaseInterface(lm);
		return MM_OK;
	}
	return MM_FAIL;
}

