#ifndef __DAMAGE_H
#define __DAMAGE_H
#include <stdbool.h>
#define I_DAMAGE "damage-6"

/*
 * arena settings:

[Damage]
IgnoreTeamDamage = 0
*/
// All callbacks run in the main thread

/** called when a tracked player is killed */
typedef void (*DamageKilledFunc)(Player *p, Player *killer, void *clos);

/** user module must set new x and y coords for this players position. */
typedef void (*DamageRespawnFunc)(Player *p, void *clos);

/** called when a tile in a tracked region takes damage. x and y are the tile,
 * firedBy is the player which fired the weapon, damage is the amount of damage
 * taken, wtype is the type of the weapon that did the damage, see packets/ppk.h
 * for weapon types.
 * if bouncingBomb is set, the bomb exploded after the last bounce
 * if the bomb was an EMP bomb, empTime tells you how long the emp lasts in
 * ticks, otherwise it is 0
 *
 */
typedef void (*TileDamageFunc)(Arena *arena, int x, int y, Player *firedBy,
                              int damagedealt, int wtype, int level,
                              bool bouncingbomb, int emptime,
                              void *clos);

/** called when a fake player takes damage. dist is how far from the center of
  * the player the bomb exploded (always 0 with bullets). firedBy is the player
  * which fired the weapon. damage is the amount of damage taken, wtype is the
  * weapon type that did the damage.
  * bouncing is set when the bomb is a bouncing bomb or bouncing bullet.
  * if the bomb was an EMP bomb, emptime tells you how long the emp lasts
  * (otherwise it is 0), this MAY not be the actual emp time if the previous emp
  * hit lasts longer.
  *
  * This callback is always called for team damage, even if
  * Damage:IgnoreTeamDamage is set
  */
typedef void (*FakeDamageFunc)(Player *fake, Player *firedBy,
                               int dist, int damagedealt, int wtype, int level,
                               bool bouncing, int emptime,
                               void *clos);

struct DamageProxData
{
	ticks_t triggered; // when was the prox bomb triggered?
	i16 dist;  // How close is this prox bomb currently to the player that
	           // triggered it? (this will only decrease)
	i16 delay; // Bomb:BombExplodeDelay
	i16 pid;   // pid of triggerer (-1 if not triggered yet). It is possible
	           // that the player no longer exists
};

enum
{
	DAMAGE_REMOVEWPN_HIT = 1, // Hit a solid tile
	DAMAGE_REMOVEWPN_HITPLAYER, // not very accurate for non fake players
	DAMAGE_REMOVEWPN_OTHERBARRELHIT, // multifire / double barrel and an other barrel hit a player
	DAMAGE_REMOVEWPN_SHIPFREQCHANGE, // the firer changed ship
	DAMAGE_REMOVEWPN_LEAVEARENA, // the firer left the arena
	DAMAGE_REMOVEWPN_EXPIRED, // bulletalivetime, etc
	DAMAGE_REMOVEWPN_SAFEZONE,
};

typedef const struct _DamageWeapon
{
	Player *p;
	i32 x; // x1000
	i32 y; // x1000
	i16 xspeed;
	i16 yspeed;
	i16 hitx; // When the bomb hits a wall, this is the tile coordinate of the wall
	i16 hity;
	u8 bounce; // bounces left
	u8 proxdistance; // prox distance for this bomb level (in pixels)
	u8 bouncebomb : 1;
	u8 empbomb : 1;
	u8 fuselit : 1; // a ship came within the prox range
	u8 padding : 5;
	u8 removereason; // only used during removal

	ticks_t startedat;
	ticks_t lastupdate;
	ticks_t alivetime;
	const struct _DamageWeapon *nextbarrel; // circular linked list
	struct Weapons wpn; // ppk.h
	struct DamageProxData prox; // Only if (wpn.type == W_PROXBOMB ||
	                            // wpn.type == W_THOR); Do not attempt to access
				    // otherwise!
} DamageWeapon;


typedef void (*DamageNewWeaponFunc)(Arena *arena, DamageWeapon *wpn);
typedef void (*DamageRemoveWeaponFunc)(Arena *arena, DamageWeapon *wpn, u8 removereason);

/// All the weapons were just updated for this arena
typedef void (*DamageWeaponsUpdatedFunc)(Arena *arena, ticks_t now);

#define FOR_EACH_DAMAGEWEAPON(wpn, arena) \
	for ( \
		link = damage->LLGetHeadWeapons(arena); \
		link && ((wpn = (DamageWeapon*) link->data, link = link->next) || 1); )


/*
	Link *link;
	DamageWeapon *wpn;
	damage->WeaponLock(arena);
	FOR_EACH_DAMAGEWEAPON(wpn, arena)
	{

	}
	damage->WeaponUnlock(arena);
*/

/** the damage interface struct */
typedef struct Idamage
{
	INTERFACE_HEAD_DECL

	/** pos will get modified if this player is effected by a weapon,
	  * if manageenergy is set, damage will keep track of the bot's energy and
	  * kills the fake player when appropiate. if not you have to do this
	  * yourself using damagefunc. (remember to reset energy in respawnfunc and
	  * shipchange)
	  *
	  * killfunc will get called when this player dies.
	  * damagefunc will get called when this player takes damage
	  * currently killfunc and damagefunc are optional,
	  * but respawnfunc is required.
	  */
	void  (*AddFake)(Player *p, struct C2SPosition *pos, bool manageenergy,
			DamageKilledFunc killfunc, DamageRespawnFunc respawnfunc,
            		FakeDamageFunc damagefunc,
            		void *clos);
        void  (*KillFake)(Player *p, Player *killer); /// causes the bot to die by killer
	void  (*RemoveFake)(Player *p);

	// Note: damage->FakePosition is no longer needed! game->FakePosition is enough

	void  (*AddRegion)(Arena *arena, Region *rgn, TileDamageFunc tilefunc, void *clos);
	void  (*RemoveRegion)(Arena *arena, Region *rgn);


	/** Not using the inbuilt ASSS callback system; These callbacks are VERY
	  * high volume
	  */
	void  (*AddWeaponCallback)(Arena *arena, DamageNewWeaponFunc addFunc,
	                         DamageRemoveWeaponFunc removeFunc,
	                         DamageWeaponsUpdatedFunc updatedFunc);
	void  (*RemoveWeaponCallback)(Arena *arena, DamageNewWeaponFunc addFunc,
	                         DamageRemoveWeaponFunc removeFunc,
	                         DamageWeaponsUpdatedFunc updatedFunc);

	/// If the below LinkedList is used, you must use the WeaponLock
	/// The weapon lock MUST NOT be active when calling any other method (AddFake, etc)
	void  (*WeaponLock)(Arena *arena);
	void  (*WeaponUnlock)(Arena *arena);
	Link* (*LLGetHeadWeapons)(Arena *arena);
} Idamage;

#endif

